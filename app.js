var express = require('express');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var cookieParser = require('cookie-parser');
var cookieSession = require('cookie-session');
var path = require('path')
var Schema = mongoose.Schema;
var http = require('http');
var app = express();
var MongoClient = require('mongodb').MongoClient;
var UserSchema = require('./src/app/schemas/userSchema');

// Configuring Passport
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var expressSession = require('express-session');
const MongoStore = require('connect-mongo')(expressSession);
const uuid = require('uuid');
const secret = uuid.v4();
app.use(express.static(path.join(__dirname, 'public')));
app.use(cookieParser());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Headers', '*');
    res.setHeader('Access-Control-Allow-Methods', '*');
    next();
});
app.use(expressSession({  secret: secret,
    name: 'graphNodeCookie',
    resave: false,
    saveUninitialized: false,
    cookie: { maxAge: 1800000 },
    store: new MongoStore({ url: 'mongodb://localhost/loginSession'  })}));

app.use(passport.initialize());
app.use(passport.session());
app.use(function(req, res, next) {
    console.log('-- session --');
    console.dir(req.session);
    console.log('-------------');
    console.log('-- cookies --');
    console.dir(req.cookies);
    console.log('-------------');
    console.log('-- signed cookies --');
    console.dir(req.signedCookies);
    console.log('-------------');
    
    next()
  });


// passport/login.js
passport.use(new LocalStrategy({
    passReqToCallback: true
},
    function (req, username, password, done) {
        console.log('user details..', username);
        console.log('password..', password);
        // check in mongo if a user with username exists or not
        UserSchema.find({ 'name': username },
            function (err, user) {
                // In case of any error, return using the done method
                if (err)
                    return done(err);
                // Username does not exist, log error & redirect back
                if (!user) {
                    console.log('User Not Found with username ' + username);
                    return done(null, false,
                        console.log('message', 'User Not found.'));
                }
                return done(null, user[0]);
            }
        );
    }));
passport.serializeUser(function (user, done) {
    console.log('inside serialize method..',user.user_id);
    done(null, user);
});

passport.deserializeUser(function (user_id, done) {
    console.log('deserialize user_id value..',user_id);
    UserSchema.findById(user_id, function(err, user) {
        console.log('deserilize setting the object in req.user..', req.user);
        done(null, user);
 });
});

// Connection URL
var url = 'mongodb://localhost:27017/userdbs';
// MongoClient.connect(url, function(err, db) {
mongoose.connect(url, function (err, db) {
    if (!err) {
        console.log("Connected to Database");
    }
    else {
        console.log("Failed to connect database");
    }
});


// user authentication
// app.post('/userAuthentication', passport.authenticate('login'), function (req, res) {
//     console.log('request object..', req.user);
//     console.log('session object..', req.session);
//     res.json({ result: req.user });
// });

app.post('/userAuthentication', function(req, res, next) { passport.authenticate('local', function(err, user, info) {
    
        if (err) {
            return next(err);
        }
    
        if (!user) {
            return res.status(401).json({
                err: info
            });
        }
    
        req.logIn(user, function(err) {
    
            if (err) {
                return res.status(500).json({
                    err: 'Could not log in user'
                });
            }
            console.log('response sending as..',req.user);
            console.log('session object..',req.session);
            res.json({ result: req.user });
            // res.status(200).json({
            //     status: 'Login successful!'
            // });
    
        });
      })(req, res, next);
    });


// create user
app.post('/createUser', function (req, res) {
    console.log('create user hits..');
    console.log(req.body.user);
    var user = req.body.user;
    var userSchema = new UserSchema({
        name: user.name,
        user_id: user.user_id,
        email_id: user.email_id,
        age: user.age,
        gender: user.gender,
        dob: user.dob
    })
    console.log('session object..', req.session);
    console.log('user object..', req.user);
    console.log('userschema object..', userSchema);
    userSchema.save(function (err, result) {
        if (err) {
            console.log('error occurs...', err);
        }
        else {
            console.log('inserted successfully', result);
            res.json({ result: 'inserted' });
        }
    });
    UserSchema.find({}, function (err, result) {
        if (err) {
            console.log('error occurs...', err);
        }
        else {
            console.log('fetched successfully', result);
        }
    })
})


// edit user
app.post('/editUser', function (req, res) {
    console.log('edit user hits..');
    console.log(req.body.user);
    var user = req.body.user;
    console.log('session object..', req.session);
    console.log('user object..', req.user);
    UserSchema.update({ "user_id": user.user_id }, { $set: req.body.user }, function (err, result) {
        if (err) {
            console.log('error occurs...', err);
        }
        else {
            console.log('updated successfully', result);
            res.json({ result: 'updated', user: req.body.user });
        }
    });
    UserSchema.find({}, function (err, result) {
        if (err) {
            console.log('error occurs...', err);
        }
        else {
            console.log('fetched successfully', result);
        }
    })
})



// fetch users
app.post('/fetchUser', function (req, res) {
    console.log('fetch user hits in my api...');
    console.log('req.user obj..', req.body.userId);
    console.log('session object..', req.session);
    console.log('user object..', req.user);
    UserSchema.find({ user_id: { $ne: req.body.userId } }, function (err, result) {
        if (err) {
            console.log('error occurs...', err);
        }
        else {
            console.log('fetched successfully', result);
        }
        res.json({ users: result });
    })
})

//  specific user fetch
app.post('/fetchUserById', function (req, res) {
    console.log('fetch user by id hits in my api...');
    console.log('req.user obj..', req.body.userId);
    console.log('session object..', req.session);
    console.log('user object..', req.user);
    UserSchema.find({ user_id: req.body.userId }, function (err, result) {
        if (err) {
            console.log('error occurs...', err);
        }
        else {
            console.log('fetched successfully', result);
        }
        res.json({ user: result });
    })
})

//  remove all documents
app.get('/removeAll', function (req, res) {
    console.log('remove all handler hits in my api...');
    UserSchema.remove({}, function (err, result) {
        if (err) {
            console.log('error occurs...', err);
        }
        else {
            console.log('all documents removed successfully', result);
        }
        res.json({ users: result });
    })
})

// remove specific documents
app.post('/removeUser', function (req, res) {
    console.log('remove user handler hits in my api...');
    console.log('passed user Id..', req.body.userId);
    console.log('session object..', req.session);
    console.log('user object..', req.user);
    UserSchema.remove({ user_id: req.body.userId }, function (err, result) {
        if (err) {
            console.log('error occurs...', err);
        }
        else {
            console.log('specified document removed successfully', result);
        }
        res.json({ result: 'removed' });
    })
})

// signout
app.get('/signout', function (req, res) {
    console.log('logged out successfully');
    console.log('session object before logout..', req.session);
    console.log('user object..', req.user);
    req.logout();
    console.log('session object after logout..', req.session);
    console.log('user object..', req.user);
    res.json({ result: 'loggedOut' });
});

app.listen(8085, function () {
    console.log('listening to the port 8085');
});
