import { Component , OnInit } from '@angular/core';
import { UserObject } from '../interfaces/login.interface';
import { UserService } from '../services/user.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-adminedit-root',
  templateUrl: './adminEdit.component.html',
  styleUrls: ['./login.component.css']
})
export class AdminEditComponent {
  public model = new UserObject('', '');
  public response: string;
  public userArray: string[];
  public loggedInUser: any;
  public tabOption: string;
  public edit: Boolean;
  constructor(public userService: UserService, public router: Router, private route: ActivatedRoute) {
       this.loggedInUser = this.userService.getUser();
       console.log('logged in user obj..', this.loggedInUser);
       this.tabOption = this.userService.getTabOption();
       if (this.tabOption === 'update') {
         this.edit = true;
       } else {
        this.edit = false;
       }
      this.userService.fetchUsers(this.loggedInUser.user_id).subscribe( data => {
          console.log('users array..', data.users);
          this.userArray = data.users;
      });
  }
  OnDelete(userId) {
      console.log('need to delete the user_id..', userId);
      this.userService.deleteUser(userId).subscribe(data => {
          console.log(data.result);
          this.userService.fetchUsers(this.loggedInUser.user_id).subscribe( data1 => {
            console.log('users array..', data1.users);
            this.userArray = data1.users;
        });
      });
  }
  OnEdit(userId) {
    console.log('need to delete the user_id..', userId);
    this.userService.fetchUserById(userId).subscribe(data => {
        console.log('return value from api..', data.user);
        this.userService.setChangingUser(data.user);
        this.userService.setAdminOptions('edit');
        this.router.navigate(['../editUser'], { relativeTo: this.route });
    });
}
}
