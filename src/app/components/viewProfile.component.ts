import { Component } from '@angular/core';
import { UserObject } from '../interfaces/login.interface';
import { UserService } from '../services/user.service';
import { Router, ActivatedRoute } from '@angular/router';
declare var jQuery: any;
@Component({
  selector: 'app-viewprofile',
  templateUrl: './viewProfile.component.html',
  styleUrls: ['./login.component.css']
})
export class ViewProfileComponent {
  public model = new UserObject('', '');
  public response: string;
  public option: string;
  public viewProfile: Boolean = false;
  constructor(public userService: UserService, public router: Router, private route: ActivatedRoute) {
     this.model = this.userService.getUser();
    console.log('view component loading...model value', this.model);
        this.option = this.userService.getOptions();
        console.log('set options..', this.option);
    if (this.option === 'viewProfile') {
        console.log('view profile options selected...');
        this.viewProfile = true;
    } else {
        console.log('edit profile options selected...');
        this.viewProfile = false;
    }
  }
  onEdit() {
      console.log('changed data...', this.model);
    this.userService.editUser(this.model).subscribe(data => {
            console.log('data', data);
            if (data.result === 'updated') {
                this.response = 'success';
                console.log('object returned as..', data.user);
                this.userService.setUser(data.user);
               console.log('service user obj set value..',  this.userService.getUser());
               this.model = this.userService.getUser();
               this.viewProfile = true;
            }else {
                this.response = 'failure';
            }

        }
    );

  }
}
