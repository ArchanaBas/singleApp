import { Component } from '@angular/core';
import { UserObject } from '../interfaces/login.interface';
import { UserService } from '../services/user.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-login-root',
  templateUrl: './createUser.component.html',
  styleUrls: ['./login.component.css']
})
export class CreateComponent {
  public model = new UserObject('', '');
  public response: string;
  public adminOption: string;
  public changingUser= new UserObject('', '');
  public edit: Boolean;
  constructor(public userService: UserService, public router: Router, private route: ActivatedRoute) {
     this.adminOption = this.userService.getAdminOptions();
     if (this.adminOption === 'edit') {
       this.edit = true;
       console.log('admin editing existing user...');
      this.changingUser = this.userService.getChangingUser();
      console.log('the changing user value..', this.changingUser[0]);
      this.model = this.changingUser[0];
     }else {
      this.model.name = null;
      this.model.user_id = null;
      this.model.email_id = null;
      this.model.age = null;
      this.model.gender = null;
      this.model.dob = null;
       this.edit = false;
     }
  }
  onSubmit() {
    this.userService.createUser(this.model).subscribe(data => {
            console.log('data', data);
            if (data.result === 'inserted') {
                this.response = 'success';
            }else {
                this.response = 'failure';
            }
            this.model.name = null;
            this.model.user_id = null;
            this.model.email_id = null;
            this.model.age = null;
            this.model.gender = null;
            this.model.dob = null;

        }
    );

  }
  onEdit() {
    console.log('changed data...', this.model);
  this.userService.editUser(this.model).subscribe(data => {
          console.log('data', data);
          this.router.navigate(['../adminUpdate'], { relativeTo: this.route });
          // if (data.result === 'updated') {
          //     this.response = 'update';
          //     this.userService.setAdminOptions('create');
          //     this.edit = false;
          //     this.model.name = null;
          //     this.model.user_id = null;
          //     this.model.email_id = null;
          //     this.model.age = null;
          //     this.model.gender = null;
          //     this.model.dob = null;
          // }else {
          //     this.response = 'failure';
          // }

      }
  );

}

}
