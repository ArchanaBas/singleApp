import { Component } from '@angular/core';
import { UserObject } from '../interfaces/login.interface';
import { UserService } from '../services/user.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-login-root',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {
  public model = new UserObject('', '');
  public errorMsg: Boolean = false;
  public adminArray: String[]= ['Archana', 'vaish'];
  constructor(public userService: UserService, public router: Router, private route: ActivatedRoute) {
    console.log('login component loading...');
  }
  onSubmit() {
    this.userService.userLogin(this.model).subscribe(data => {
      console.log('data', data);
      if (data.result) {
        console.log('valid user..', data.result);
        this.userService.setUser(data.result);
        console.log('admin index value..', this.adminArray.indexOf(data.result.name));
        if (this.adminArray.indexOf(data.result.name) !== -1) {
          this.router.navigate(['admin']);
        }else {
          this.router.navigate(['user']);
        }
      } else {
        console.log('invalid user..');
        this.errorMsg = true;
        this.model.name = '';
        this.model.password = '';
      }
    }
    );
  }
}
