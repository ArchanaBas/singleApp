import {Component} from '@angular/core';
import {UserObject} from '../interfaces/login.interface';
import { UserService } from '../services/user.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
    selector: 'app-user-page',
    templateUrl: './admin.component.html',
    styleUrls: ['./user.component.css']
})

export class AdminComponent {
    public model = new UserObject('', '');
    constructor(public userService: UserService,  public router: Router, private route: ActivatedRoute) {
        this.model.name = '';
        this.model.password = '';
        console.log('login component loading...');
        this.model = this.userService.getUser();
        console.log('create component loading...model value', this.model);
    }
    public logout() {
        this.userService.logout().subscribe(data => {
            console.log('data', data.result);
            this.router.navigate(['home']);
        });

    }
    OnClick(value) {
        console.log('clicked value..', value);
        this.userService.setTabOption(value);
        if (value === 'create') {
            this.userService.setAdminOptions(value);
            this.router.navigate(['create'], { relativeTo: this.route });
        } else if (value === 'update') {
            this.router.navigate(['adminUpdate'], { relativeTo: this.route });
        } else if (value === 'delete') {
            this.router.navigate(['adminDelete'], { relativeTo: this.route });
        } else {
            // this.router.navigate(['../report'], { relativeTo: this.route });
        }

    }
}
