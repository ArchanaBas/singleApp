import {Component} from '@angular/core';
import {UserObject} from '../interfaces/login.interface';
import { UserService } from '../services/user.service';
import { Router, ActivatedRoute } from '@angular/router';
@Component({
    selector: 'app-option-page',
    templateUrl: './options.component.html',
    styleUrls: ['./user.component.css']
})

export class OptionsComponent {
    public model = new UserObject('', '');
    constructor(public userService: UserService,  public router: Router, private route: ActivatedRoute) {
        this.model.name = '';
        this.model.password = '';
        console.log('login component loading...');
        this.model = this.userService.getUser();
        console.log('create component loading...model value', this.model);
    }
    Options(SelectedOptions) {
        console.log('selected options..', SelectedOptions);
        if (SelectedOptions === 'viewProfile' ) {
            this.userService.setOptions('viewProfile');
            this.router.navigate(['../viewProfile'], { relativeTo: this.route });

        }else if (SelectedOptions === 'editProfile') {
            this.userService.setOptions('editProfile');
            this.router.navigate(['../viewProfile'], { relativeTo: this.route });
        }else {
            this.userService.setOptions('otherUsers');
            this.router.navigate(['../otherUsers'], { relativeTo: this.route });
        }
    }
}
