import { Component } from '@angular/core';
import { UserObject } from '../interfaces/login.interface';
import { UserService } from '../services/user.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-other-root',
  templateUrl: './otherUser.component.html',
  styleUrls: ['./login.component.css']
})
export class OtherUsersComponent {
  public model = new UserObject('', '');
  public response: string;
  public userArray: string[];
  public loggedInUser: any;
  constructor(public userService: UserService, public router: Router, private route: ActivatedRoute) {
       this.loggedInUser = this.userService.getUser();
       console.log('logged in user obj..', this.loggedInUser);
      this.userService.fetchUsers(this.loggedInUser.user_id).subscribe( data => {
          console.log('users array..', data.users);
          this.userArray = data.users;
      });
  }
}
