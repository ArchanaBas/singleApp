import { Component } from '@angular/core';
import { UserObject } from '../interfaces/login.interface';
import { UserService } from '../services/user.service';
import { Router, ActivatedRoute } from '@angular/router';
@Component({
    selector: 'app-user-page',
    templateUrl: './user.component.html',
    styleUrls: ['./user.component.css']
})

export class UserComponent {
    public model = new UserObject('', '');
    constructor(public userService: UserService, public router: Router, private route: ActivatedRoute) {
        console.log('login component loading...');
        this.model = this.userService.getUser();
        console.log('create component loading...model value', this.model);
    }
    public logout() {
        this.userService.logout().subscribe(data => {
            console.log('data', data.result);
            this.router.navigate(['home']);
        });
    }
    public Options() {
        console.log('home clicked...');
        this.router.navigate(['options'], { relativeTo: this.route });
}
}
