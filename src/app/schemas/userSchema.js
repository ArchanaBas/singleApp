var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var UserSchema = new Schema({
    name: { type: String },
    user_id: { type: String, Unique: true, required: true },
    email_id: { type: String, Unique: true, required: true },
    age: { type: Number },
    gender: { type: String },
    dob: { type: String }
});

module.exports = mongoose.model('UserSchema', UserSchema);