import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Headers } from '@angular/http';
import { UserObject } from '../interfaces/login.interface';
import 'rxjs/Rx';
@Injectable()
export class UserService {
    public user = new UserObject('', '');
    public userArray: any[];
    public option: string;
    public adminOption: string;
    public tabOption: string;
    public changingUser = new UserObject('', '');
    constructor(private http: Http) {
    }

    //  user authentication
    public userLogin(user) {
        console.log('user object..', user);
        const body = JSON.stringify({ username: user.userName, password: user.password });
        console.log('body content sending for authentication...', body);
        const headers = new Headers({ 'Content-Type': 'application/json' });
        return this.http.post(`http://localhost:8085/userAuthentication`, body, { headers: headers })
            .map((response: Response) => response.json());
    }

    //  create new user by admin
    public createUser(user) {
        console.log('user object..', user);
        const body = JSON.stringify({ user: user });
        const headers = new Headers({ 'Content-Type': 'application/json' });
        return this.http.post(`http://localhost:8085/createUser`, body, { headers: headers })
            .map((response: Response) => response.json());
    }
    //  edit users in userLogin
    public editUser(user) {
        console.log('user object..', user);
        const body = JSON.stringify({ user: user });
        const headers = new Headers({ 'Content-Type': 'application/json' });
        return this.http.post(`http://localhost:8085/editUser`, body, { headers: headers })
            .map((response: Response) => response.json());
    }
    public setUser(user) {
        this.user = user;
    }
    public getUser() {
        return this.user;
    }
    public setOptions(option) {
        this.option = option;
    }
    public getOptions() {
        return this.option;
    }
    public setAdminOptions(option) {
        this.adminOption = option;
    }
    public getAdminOptions() {
        return this.adminOption;
    }
    public setTabOption(option) {
        this.tabOption = option;
    }
    public getTabOption() {
        return this.tabOption;
    }

    public setChangingUser(user) {
        this.changingUser = user;
    }

    public getChangingUser() {
        return this.changingUser;
    }
    // fetching all the users
    public fetchUsers(userId) {
        console.log('fetchusers...in service');
        console.log('user object..', userId);
        const body = JSON.stringify({ userId: userId });
        const headers = new Headers({ 'Content-Type': 'application/json' });
        return this.http.post(`http://localhost:8085/fetchUser`, body, { headers: headers })
        .map((response: Response) => response.json());
    }

    //  signout
    public logout() {
        console.log('logout fnctn n service');
        return this.http.get(`http://localhost:8085/signout`)
        .map((response: Response) => response.json());
    }

    //  remove specific record
    public deleteUser(userId) {
        console.log('fetchusers...in service');
        console.log('user object..', userId);
        const body = JSON.stringify({ userId: userId });
        const headers = new Headers({ 'Content-Type': 'application/json' });
        return this.http.post(`http://localhost:8085/removeUser`, body, { headers: headers })
        .map((response: Response) => response.json());
    }

    //  fetch user by id
    public fetchUserById(userId) {
        console.log('fetchusers...in service');
        console.log('user object..', userId);
        const body = JSON.stringify({ userId: userId });
        const headers = new Headers({ 'Content-Type': 'application/json' });
        return this.http.post(`http://localhost:8085/fetchUserById`, body, { headers: headers })
        .map((response: Response) => response.json());
    }
}
