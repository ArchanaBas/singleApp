import { Routes, RouterModule } from '@angular/router';
import { UserComponent } from './components/user.component';
import { LoginComponent } from './components/login.component';
import { AdminComponent } from './components/admin.component';
import { CreateComponent } from './components/createUser.component';
import { OptionsComponent } from './components/options.component';
import { ViewProfileComponent } from './components/viewProfile.component';
import { OtherUsersComponent } from './components/otherUser.component';
import { AdminEditComponent } from './components/adminEdit.component';


export const APP_ROUTES: Routes = [
    { path: '', redirectTo: '/home', pathMatch: 'full' },
    { path: 'home', component: LoginComponent },
    {
        path: 'user', component: UserComponent, children: [
            { path: '', redirectTo: 'options', pathMatch: 'full' },
            { path: 'options', component: OptionsComponent },
            { path: 'viewProfile', component: ViewProfileComponent },
            { path: 'otherUsers', component: OtherUsersComponent }
        ]
    },
    {
        path: 'admin', component: AdminComponent, children: [
            { path: '', redirectTo: 'create', pathMatch: 'full' },
            { path: 'create', component: CreateComponent },
            { path: 'editUser', component: CreateComponent },
            { path: 'adminUpdate', component:  AdminEditComponent},
            { path: 'adminDelete', component:  AdminEditComponent}
        ]
    }
];

export const navigatableComponents = [
    LoginComponent,
    UserComponent,
    AdminComponent,
    CreateComponent
];
export const routing = RouterModule.forRoot(APP_ROUTES);

export class AppRoutingModule { }
