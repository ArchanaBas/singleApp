import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { APP_ROUTES, navigatableComponents } from './app.routing';
import { AppComponent } from './app.component';
import { UserComponent } from './components/user.component';
import { LoginComponent } from './components/login.component';
import { AdminComponent } from './components/admin.component';
import { UserService } from './services/user.service';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';
import { CreateComponent } from './components/createUser.component';
import { OptionsComponent } from './components/options.component';
import { ViewProfileComponent } from './components/viewProfile.component';
import { OtherUsersComponent } from './components/otherUser.component';
import { AdminEditComponent } from './components/adminEdit.component';


@NgModule({
  declarations: [
    AppComponent,
    UserComponent,
    LoginComponent,
    AdminComponent,
    CreateComponent,
    ViewProfileComponent,
    OptionsComponent,
    OtherUsersComponent,
    AdminEditComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot(APP_ROUTES)
  ],
  providers: [UserService],
  bootstrap: [AppComponent]
})
export class AppModule { }
